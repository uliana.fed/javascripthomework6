function createNewUser() {
  const newUser = {
    firstName: prompt("Введіть ваше ім'я"),
    lastName: prompt("Введіть ваше прізвище"),
    birthday: prompt("Введіть вашу дату народження у форматі dd.mm.yyyy"),
    getLogin() {
      const firstLetter = this.firstName.charAt(0).toLowerCase();
      return `${firstLetter}${this.lastName.toLowerCase()}`
    },
    getAge() {
      const present = new Date();
      const birthDate = new Date(this.birthday.split('.').reverse().join('-'));
      let age = present.getFullYear() - birthDate.getFullYear();
      const month = present.getMonth() - birthDate.getMonth();
      if (month < 0 || (month === 0 && present.getDate() < birthDate.getDate())) {
        age--;
      }
      return age;
    },
    getPassword() {
      const firstLetterPassword = this.firstName.charAt(0).toUpperCase();
      const year = this.birthday.split('.')[2];
      return `${firstLetterPassword}${this.lastName.toLowerCase()}${year} `
    }
  };
  
  return newUser;
}

const anotherUser = createNewUser();
console.log(anotherUser);
console.log(anotherUser.getAge());
console.log(anotherUser.getPassword());